# npm package starter

A boilerplate to help you get started creating a npm package and deploy it on the GitLab package registry

## How to use

```
git clone git@gitlab.com:nmezzopera/npm-package-starter.git npm-package-starter
cd npm-package-starter
npm i
modify package.json#L35 with the correct project scope ( where you want to deploy the package )
modify .npmrc_template with the correct project ids
rename .npmrc_template to .npmrc
npm publish
```

More info: https://docs.gitlab.com/ee/user/packages/npm_registry/index.html#uploading-packages

to quickly deploy several version of the package:

```
for run in {1..50}
do
npm version patch --force && npm publish
done
```